# Cycparser

[![pipeline status](https://gitlab.com/qtb-hhu/cycparser-py/badges/main/pipeline.svg)](https://gitlab.com/qtb-hhu/cycparser-py/-/commits/main)
[![coverage report](https://gitlab.com/qtb-hhu/cycparser-py/badges/main/coverage.svg)](https://gitlab.com/qtb-hhu/cycparser-py/-/commits/main)
[![PyPi](https://img.shields.io/pypi/v/cycparser)](https://pypi.org/project/cycparser/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)
[![Downloads](https://pepy.tech/badge/cycparser)](https://pepy.tech/project/cycparser)

Used in the [moped](https://gitlab.com/qtb-hhu/moped) project
