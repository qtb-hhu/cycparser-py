from cycparser.parsing import parse_compounds
from cycparser.repairing import get_highest_compound_type


def test_add_cpd_type_single() -> None:
    cpds = parse_compounds(type_map={}, file=["UNIQUE-ID - cpd1", "TYPES - LIGNAN"])
    types = get_highest_compound_type(cpds)
    assert cpds["cpd1_c"].types == ["LIGNAN"]
    assert types == {"LIGNAN_c": ["cpd1_c"]}


def test_add_cpd_type_double() -> None:
    cpds = parse_compounds(
        type_map={}, file=["UNIQUE-ID - cpd1", "TYPES - LIGNAN", "TYPES - Toxins"]
    )
    types = get_highest_compound_type(cpds)
    assert cpds["cpd1_c"].types == ["LIGNAN", "Toxins"]
    assert types == {"Toxins_c": ["cpd1_c"]}
