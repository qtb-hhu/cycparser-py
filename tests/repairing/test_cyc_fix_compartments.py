from __future__ import annotations

from cycparser import COMPARTMENT_MAP, COMPARTMENT_SUFFIXES, Compound, Reaction
from cycparser.repairing.fix_create_compartment_variants import (
    _split_location,
    fix_create_compartment_variants,
)

import pytest


def test_split_location() -> None:
    assert _split_location(location="CCO-CYTOSOL", compartment_map=COMPARTMENT_MAP) == {
        "CCO-OUT": "CYTOSOL",
        "CCO-IN": "CYTOSOL",
    }
    assert _split_location(
        location="CCO-EXTRACELLULAR", compartment_map=COMPARTMENT_MAP
    ) == {
        "CCO-OUT": "EXTRACELLULAR",
        "CCO-IN": "CYTOSOL",
    }
    assert _split_location(
        location="CCO-EXTRACELLULAR-CCO-CYTOSOL", compartment_map=COMPARTMENT_MAP
    ) == {
        "CCO-OUT": "EXTRACELLULAR",
        "CCO-IN": "CYTOSOL",
    }
    assert _split_location(
        location="CCO-CYTOSOL-CCO-EXTRACELLULAR", compartment_map=COMPARTMENT_MAP
    ) == {
        "CCO-OUT": "CYTOSOL",
        "CCO-IN": "EXTRACELLULAR",
    }


def test_fix_compartments_both_in() -> None:
    compounds = {
        "cpd1_c": Compound(base_id="cpd1", id="cpd1", formula={"C": 1}, charge=1),
        "cpd2_c": Compound(base_id="cpd2", id="cpd2", formula={"C": 1}, charge=1),
    }
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            locations=[],
        )
    }
    reactions = fix_create_compartment_variants(
        parse_reactions=reactions,
        compounds=compounds,
        compartment_map=COMPARTMENT_MAP,
        compartment_suffixes=COMPARTMENT_SUFFIXES,
    )
    assert compounds["cpd1_c"].compartment == "CYTOSOL"
    assert compounds["cpd2_c"].compartment == "CYTOSOL"
    assert reactions["RXN1_c"].substrates == {"cpd1_c": -1}
    assert reactions["RXN1_c"].products == {"cpd2_c": 1}


def test_fix_compartments_both_out() -> None:
    compounds = {
        "cpd1_c": Compound(base_id="cpd1", id="cpd1", formula={"C": 1}, charge=1),
        "cpd2_c": Compound(base_id="cpd2", id="cpd2", formula={"C": 1}, charge=1),
    }
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-OUT"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-OUT"},
            locations=[],
        )
    }
    reactions = fix_create_compartment_variants(
        parse_reactions=reactions,
        compounds=compounds,
        compartment_map=COMPARTMENT_MAP,
        compartment_suffixes=COMPARTMENT_SUFFIXES,
    )
    assert compounds["cpd1_c"].compartment == "CYTOSOL"
    assert compounds["cpd2_c"].compartment == "CYTOSOL"
    assert compounds["cpd1_e"].compartment == "EXTRACELLULAR"
    assert compounds["cpd2_e"].compartment == "EXTRACELLULAR"
    assert reactions["RXN1_e"].substrates == {"cpd1_e": -1}
    assert reactions["RXN1_e"].products == {"cpd2_e": 1}
    with pytest.raises(KeyError):
        reactions["RXN1"]


def test_fix_compartments_in_out() -> None:
    compounds = {
        "cpd1_c": Compound(base_id="cpd1", id="cpd1", formula={"C": 1}, charge=1),
        "cpd2_c": Compound(base_id="cpd2", id="cpd2", formula={"C": 1}, charge=1),
    }
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-OUT"},
            locations=[],
        )
    }
    reactions = fix_create_compartment_variants(
        parse_reactions=reactions,
        compounds=compounds,
        compartment_map=COMPARTMENT_MAP,
        compartment_suffixes=COMPARTMENT_SUFFIXES,
    )
    assert compounds["cpd1_c"].compartment == "CYTOSOL"
    assert compounds["cpd2_c"].compartment == "CYTOSOL"
    assert compounds["cpd2_e"].compartment == "EXTRACELLULAR"
    assert reactions["RXN1_c_e"].substrates == {"cpd1_c": -1}
    assert reactions["RXN1_c_e"].products == {"cpd2_e": 1}
    with pytest.raises(KeyError):
        reactions["RXN1"]


def test_fix_compartments_out_in() -> None:
    compounds = {
        "cpd1_c": Compound(base_id="cpd1", id="cpd1", formula={"C": 1}, charge=1),
        "cpd2_c": Compound(base_id="cpd2", id="cpd2", formula={"C": 1}, charge=1),
    }
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-OUT"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            locations=[],
        )
    }
    reactions = fix_create_compartment_variants(
        parse_reactions=reactions,
        compounds=compounds,
        compartment_map=COMPARTMENT_MAP,
        compartment_suffixes=COMPARTMENT_SUFFIXES,
    )
    assert compounds["cpd1_c"].compartment == "CYTOSOL"
    assert compounds["cpd2_c"].compartment == "CYTOSOL"
    assert compounds["cpd1_e"].compartment == "EXTRACELLULAR"
    assert reactions["RXN1_c_e"].substrates == {"cpd1_e": -1}
    assert reactions["RXN1_c_e"].products == {"cpd2_c": 1}
    with pytest.raises(KeyError):
        reactions["RXN1"]


def test_fix_compartments_periplasm() -> None:
    locations = [
        "CCO-CHL-THY-MEM",
        "CCO-MIT-IMEM",
        "CCO-OUTER-MEM",
        "CCO-PERI-BAC",
        "CCO-PERI-BAC-GN",
        "CCO-PERIPLASM",
        "CCO-PLASMA-MEM",
        "CCO-PLAST-IMEM",
        "CCO-PM-ANIMAL",
        "CCO-PM-BAC-ACT",
        "CCO-PM-BAC-NEG",
        "CCO-PM-BAC-POS",
        "CCO-RGH-ER-MEM",
        "CCO-VAC-MEM",
    ]
    for location in locations:
        compounds = {
            "cpd1_c": Compound(base_id="cpd1", id="cpd1", formula={"C": 1}, charge=1),
            "cpd2_c": Compound(base_id="cpd2", id="cpd2", formula={"C": 1}, charge=1),
        }
        reactions = {
            "RXN1": Reaction(
                id="RXN1",
                base_id="RXN1",
                substrates={"cpd1_c": -1},
                substrate_compartments={"cpd1_c": "CCO-IN"},
                products={"cpd2_c": 1},
                product_compartments={"cpd2_c": "CCO-OUT"},
                locations=[location],
            )
        }
        reactions = fix_create_compartment_variants(
            parse_reactions=reactions,
            compounds=compounds,
            compartment_map=COMPARTMENT_MAP,
            compartment_suffixes=COMPARTMENT_SUFFIXES,
        )
        assert compounds["cpd1_c"].compartment == "CYTOSOL"
        assert compounds["cpd2_c"].compartment == "CYTOSOL"
        assert compounds["cpd2_p"].compartment == "PERIPLASM"
        assert reactions["RXN1_c_p"].substrates == {"cpd1_c": -1}
        assert reactions["RXN1_c_p"].products == {"cpd2_p": 1}
        with pytest.raises(KeyError):
            reactions["RXN1"]


def test_fix_compartments_extracellular() -> None:
    locations = ["CCO-EXTRACELLULAR"]
    for location in locations:
        compounds = {
            "cpd1_c": Compound(base_id="cpd1", id="cpd1", formula={"C": 1}, charge=1),
            "cpd2_c": Compound(base_id="cpd2", id="cpd2", formula={"C": 1}, charge=1),
        }
        reactions = {
            "RXN1": Reaction(
                id="RXN1",
                base_id="RXN1",
                substrates={"cpd1_c": -1},
                substrate_compartments={"cpd1_c": "CCO-IN"},
                products={"cpd2_c": 1},
                product_compartments={"cpd2_c": "CCO-OUT"},
                locations=[location],
            )
        }
        reactions = fix_create_compartment_variants(
            parse_reactions=reactions,
            compounds=compounds,
            compartment_map=COMPARTMENT_MAP,
            compartment_suffixes=COMPARTMENT_SUFFIXES,
        )
        assert compounds["cpd1_c"].compartment == "CYTOSOL"
        assert compounds["cpd2_c"].compartment == "CYTOSOL"
        assert compounds["cpd2_e"].compartment == "EXTRACELLULAR"
        assert reactions["RXN1_c_e"].substrates == {"cpd1_c": -1}
        assert reactions["RXN1_c_e"].products == {"cpd2_e": 1}
        with pytest.raises(KeyError):
            reactions["RXN1"]


def test_fix_compartments_transporters_c_c() -> None:
    locations = [
        "CCO-CYTOSOL-CCO-MIT-LUM",
        "CCO-CYTOSOL-CCO-VAC-LUM",
        "CCO-MIT-LUM-CCO-CYTOSOL",
        "CCO-PEROX-LUM-CCO-CYTOSOL",
        "CCO-RGH-ER-LUM-CCO-CYTOSOL",
        "CCO-CYTOSOL",
        "CCO-IN",
        "CCO-LYS-LUM",
        "CCO-MIT-LUM",
        "CCO-GOLGI-LUM",
        "CCO-PEROX-LUM",
        "CCO-RGH-ER-LUM",
    ]
    for location in locations:
        compounds = {
            "cpd1_c": Compound(base_id="cpd1", id="cpd1", formula={"C": 1}, charge=1),
            "cpd2_c": Compound(base_id="cpd2", id="cpd2", formula={"C": 1}, charge=1),
        }
        reactions = {
            "RXN1": Reaction(
                id="RXN1",
                base_id="RXN1",
                substrates={"cpd1_c": -1},
                substrate_compartments={"cpd1_c": "CCO-IN"},
                products={"cpd2_c": 1},
                product_compartments={"cpd2_c": "CCO-OUT"},
                locations=[location],
            )
        }
        reactions = fix_create_compartment_variants(
            parse_reactions=reactions,
            compounds=compounds,
            compartment_map=COMPARTMENT_MAP,
            compartment_suffixes=COMPARTMENT_SUFFIXES,
        )
        assert compounds["cpd1_c"].compartment == "CYTOSOL"
        assert compounds["cpd2_c"].compartment == "CYTOSOL"
        assert reactions["RXN1_c"].substrates == {"cpd1_c": -1}
        assert reactions["RXN1_c"].products == {"cpd2_c": 1}
        with pytest.raises(KeyError):
            reactions["RXN1"]


def test_fix_compartments_transporters_c_p() -> None:
    locations = [
        "CCO-PERI-BAC-CCO-CYTOSOL",
        "CCO-PERI-BAC-CCO-IN",
    ]
    for location in locations:
        compounds = {
            "cpd1_c": Compound(base_id="cpd1", id="cpd1", formula={"C": 1}, charge=1),
            "cpd2_c": Compound(base_id="cpd2", id="cpd2", formula={"C": 1}, charge=1),
        }
        reactions = {
            "RXN1": Reaction(
                id="RXN1",
                base_id="RXN1",
                substrates={"cpd1_c": -1},
                substrate_compartments={"cpd1_c": "CCO-IN"},
                products={"cpd2_c": 1},
                product_compartments={"cpd2_c": "CCO-OUT"},
                locations=[location],
            )
        }
        reactions = fix_create_compartment_variants(
            parse_reactions=reactions,
            compounds=compounds,
            compartment_map=COMPARTMENT_MAP,
            compartment_suffixes=COMPARTMENT_SUFFIXES,
        )
        assert compounds["cpd1_c"].compartment == "CYTOSOL"
        assert compounds["cpd2_c"].compartment == "CYTOSOL"
        assert compounds["cpd2_p"].compartment == "PERIPLASM"
        assert reactions["RXN1_c_p"].substrates == {"cpd1_c": -1}
        assert reactions["RXN1_c_p"].products == {"cpd2_p": 1}
        with pytest.raises(KeyError):
            reactions["RXN1"]


def test_fix_compartments_transporters_p_p() -> None:
    locations = [
        "CCO-PERI-BAC-CCO-PERI-BAC",
    ]
    for location in locations:
        compounds = {
            "cpd1_c": Compound(base_id="cpd1", id="cpd1", formula={"C": 1}, charge=1),
            "cpd2_c": Compound(base_id="cpd2", id="cpd2", formula={"C": 1}, charge=1),
        }
        reactions = {
            "RXN1": Reaction(
                id="RXN1",
                base_id="RXN1",
                substrates={"cpd1_c": -1},
                substrate_compartments={"cpd1_c": "CCO-IN"},
                products={"cpd2_c": 1},
                product_compartments={"cpd2_c": "CCO-OUT"},
                locations=[location],
                gibbs0=0,
            )
        }
        reactions = fix_create_compartment_variants(
            parse_reactions=reactions,
            compounds=compounds,
            compartment_map=COMPARTMENT_MAP,
            compartment_suffixes=COMPARTMENT_SUFFIXES,
        )
        assert compounds["cpd1_c"].compartment == "CYTOSOL"
        assert compounds["cpd2_c"].compartment == "CYTOSOL"
        assert compounds["cpd1_p"].compartment == "PERIPLASM"
        assert compounds["cpd2_p"].compartment == "PERIPLASM"
        assert reactions["RXN1_p"].substrates == {"cpd1_p": -1}
        assert reactions["RXN1_p"].products == {"cpd2_p": 1}
        with pytest.raises(KeyError):
            reactions["RXN1"]


def test_fix_compartments_transporters_p_c() -> None:
    locations = [
        "CCO-CYTOSOL-CCO-PLASTID-STR",
        "CCO-CYTOSOL-CCO-VESICLE",
    ]
    for location in locations:
        compounds = {
            "cpd1_c": Compound(
                id="cpd1_c",
                base_id="cpd1",
                formula={"C": 1},
                charge=1,
            ),
            "cpd2_c": Compound(
                id="cpd2_c",
                base_id="cpd2",
                formula={"C": 1},
                charge=1,
            ),
        }
        reactions = {
            "RXN1": Reaction(
                id="RXN1",
                base_id="RXN1",
                substrates={"cpd1_c": -1},
                substrate_compartments={"cpd1_c": "CCO-IN"},
                products={"cpd2_c": 1},
                product_compartments={"cpd2_c": "CCO-OUT"},
                locations=[location],
            )
        }
        reactions = fix_create_compartment_variants(
            parse_reactions=reactions,
            compounds=compounds,
            compartment_map=COMPARTMENT_MAP,
            compartment_suffixes=COMPARTMENT_SUFFIXES,
        )
        assert compounds["cpd1_c"].compartment == "CYTOSOL"
        assert compounds["cpd2_c"].compartment == "CYTOSOL"
        assert compounds["cpd1_p"].compartment == "PERIPLASM"
        assert reactions["RXN1_p_c"].substrates == {"cpd1_p": -1}
        assert reactions["RXN1_p_c"].products == {"cpd2_c": 1}
        with pytest.raises(KeyError):
            reactions["RXN1"]


def test_fix_compartments_transporters_c_e() -> None:
    locations = [
        "CCO-EXTRACELLULAR-CCO-CYTOSOL",
        "CCO-EXTRACELLULAR-CCO-IN",
        "CCO-EXTRACELLULAR-CCO-UNKNOWN-SPACE",
    ]
    for location in locations:
        compounds = {
            "cpd1_c": Compound(base_id="cpd1", id="cpd1", formula={"C": 1}, charge=1),
            "cpd2_c": Compound(base_id="cpd2", id="cpd2", formula={"C": 1}, charge=1),
        }
        reactions = {
            "RXN1": Reaction(
                id="RXN1",
                base_id="RXN1",
                substrates={"cpd1_c": -1},
                substrate_compartments={"cpd1_c": "CCO-IN"},
                products={"cpd2_c": 1},
                product_compartments={"cpd2_c": "CCO-OUT"},
                locations=[location],
            ),
        }
        reactions = fix_create_compartment_variants(
            parse_reactions=reactions,
            compounds=compounds,
            compartment_map=COMPARTMENT_MAP,
            compartment_suffixes=COMPARTMENT_SUFFIXES,
        )
        assert compounds["cpd1_c"].compartment == "CYTOSOL"
        assert compounds["cpd2_c"].compartment == "CYTOSOL"
        assert compounds["cpd2_e"].compartment == "EXTRACELLULAR"
        assert reactions["RXN1_c_e"].substrates == {"cpd1_c": -1}
        assert reactions["RXN1_c_e"].products == {"cpd2_e": 1}
        with pytest.raises(KeyError):
            reactions["RXN1"]


def test_fix_compartments_transporters_e_p() -> None:
    locations = ["CCO-OUT-CCO-EXTRACELLULAR"]
    for location in locations:
        compounds = {
            "cpd1_c": Compound(base_id="cpd1", id="cpd1", formula={"C": 1}, charge=1),
            "cpd2_c": Compound(base_id="cpd2", id="cpd2", formula={"C": 1}, charge=1),
        }
        reactions = {
            "RXN1": Reaction(
                id="RXN1",
                base_id="RXN1",
                substrates={"cpd1_c": -1},
                substrate_compartments={"cpd1_c": "CCO-IN"},
                products={"cpd2_c": 1},
                product_compartments={"cpd2_c": "CCO-OUT"},
                locations=[location],
            )
        }
        reactions = fix_create_compartment_variants(
            parse_reactions=reactions,
            compounds=compounds,
            compartment_map=COMPARTMENT_MAP,
            compartment_suffixes=COMPARTMENT_SUFFIXES,
        )
        assert compounds["cpd1_c"].compartment == "CYTOSOL"
        assert compounds["cpd2_c"].compartment == "CYTOSOL"
        assert compounds["cpd1_e"].compartment == "EXTRACELLULAR"
        assert compounds["cpd2_p"].compartment == "PERIPLASM"
        assert reactions["RXN1_e_p"].substrates == {"cpd1_e": -1}
        assert reactions["RXN1_e_p"].products == {"cpd2_p": 1}
        with pytest.raises(KeyError):
            reactions["RXN1"]
