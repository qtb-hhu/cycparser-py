from __future__ import annotations

from cycparser import Reaction
from cycparser.repairing.fix_unify_reaction_direction import (
    _reverse_stoichiometry,
    fix_unify_reaction_direction,
)


def test_reverse_stoichiometry() -> None:
    rxn = Reaction(
        id="RXN1",
        base_id="RXN1",
        substrates={"cpd1_c": -1},
        substrate_compartments={"cpd1_c": "CCO-IN"},
        products={"cpd2_c": 1},
        product_compartments={"cpd2_c": "CCO-IN"},
        gibbs0=-10,
    )
    _reverse_stoichiometry(rxn)
    assert rxn.substrates == {"cpd2_c": -1}
    assert rxn.substrate_compartments == {"cpd2_c": "CCO-IN"}
    assert rxn.products == {"cpd1_c": 1}
    assert rxn.product_compartments == {"cpd1_c": "CCO-IN"}
    assert rxn.gibbs0 == 10


def test_unify_direction_left_to_right() -> None:
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="LEFT-TO-RIGHT",
            bounds=(0, 1000),
        )
    }
    reactions = fix_unify_reaction_direction(reactions)

    rxn = reactions["RXN1"]
    assert rxn.substrates == {"cpd1_c": -1}
    assert rxn.substrate_compartments == {"cpd1_c": "CCO-IN"}
    assert rxn.products == {"cpd2_c": 1}
    assert rxn.product_compartments == {"cpd2_c": "CCO-IN"}
    assert rxn.gibbs0 == -10
    assert rxn.reversible is False
    assert rxn.bounds == (0, 1000)


def test_unify_direction_physiol_left_to_right() -> None:
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="PHYSIOL-LEFT-TO-RIGHT",
            bounds=(0, 1000),
        )
    }
    reactions = fix_unify_reaction_direction(reactions)
    rxn = reactions["RXN1"]
    assert rxn.substrates == {"cpd1_c": -1}
    assert rxn.substrate_compartments == {"cpd1_c": "CCO-IN"}
    assert rxn.products == {"cpd2_c": 1}
    assert rxn.product_compartments == {"cpd2_c": "CCO-IN"}
    assert rxn.gibbs0 == -10
    assert rxn.reversible is False
    assert rxn.bounds == (0, 1000)


def test_unify_direction_irrev_left_to_right() -> None:
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-LEFT-TO-RIGHT",
            bounds=(0, 1000),
        )
    }
    reactions = fix_unify_reaction_direction(reactions)
    rxn = reactions["RXN1"]
    assert rxn.substrates == {"cpd1_c": -1}
    assert rxn.substrate_compartments == {"cpd1_c": "CCO-IN"}
    assert rxn.products == {"cpd2_c": 1}
    assert rxn.product_compartments == {"cpd2_c": "CCO-IN"}
    assert rxn.gibbs0 == -10
    assert rxn.reversible is False
    assert rxn.bounds == (0, 1000)


def test_unify_direction_reversible() -> None:
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            reversible=True,
            direction="REVERSIBLE",
            bounds=(-1000, 1000),
        )
    }
    reactions = fix_unify_reaction_direction(reactions)
    rxn = reactions["RXN1"]
    assert rxn.substrates == {"cpd1_c": -1}
    assert rxn.substrate_compartments == {"cpd1_c": "CCO-IN"}
    assert rxn.products == {"cpd2_c": 1}
    assert rxn.product_compartments == {"cpd2_c": "CCO-IN"}
    assert rxn.gibbs0 == -10
    assert rxn.bounds == (-1000, 1000)


def test_unify_direction_right_to_left() -> None:
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    reactions = fix_unify_reaction_direction(reactions)
    rxn = reactions["RXN1"]
    assert rxn.substrates == {"cpd2_c": -1}
    assert rxn.substrate_compartments == {"cpd2_c": "CCO-IN"}
    assert rxn.products == {"cpd1_c": 1}
    assert rxn.product_compartments == {"cpd1_c": "CCO-IN"}
    assert rxn.gibbs0 == 10
    assert rxn.reversible is False
    assert rxn.bounds == (0, 1000)


def test_unify_direction_physiol_right_to_left() -> None:
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="PHYSIOL-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    reactions = fix_unify_reaction_direction(reactions)
    rxn = reactions["RXN1"]
    assert rxn.substrates == {"cpd2_c": -1}
    assert rxn.substrate_compartments == {"cpd2_c": "CCO-IN"}
    assert rxn.products == {"cpd1_c": 1}
    assert rxn.product_compartments == {"cpd1_c": "CCO-IN"}
    assert rxn.gibbs0 == 10
    assert rxn.reversible is False
    assert rxn.bounds == (0, 1000)


def test_unify_direction_irrev_right_to_left() -> None:
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    reactions = fix_unify_reaction_direction(reactions)
    rxn = reactions["RXN1"]
    assert rxn.substrates == {"cpd2_c": -1}
    assert rxn.substrate_compartments == {"cpd2_c": "CCO-IN"}
    assert rxn.products == {"cpd1_c": 1}
    assert rxn.product_compartments == {"cpd1_c": "CCO-IN"}
    assert rxn.gibbs0 == 10
    assert rxn.reversible is False
    assert rxn.bounds == (0, 1000)


def test_unify_direction_garbage() -> None:
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="GARBAGE",
            bounds=(0, 1000),
        )
    }
    reactions = fix_unify_reaction_direction(reactions)
    rxn = reactions["RXN1"]
    assert rxn.substrates == {"cpd1_c": -1}
    assert rxn.substrate_compartments == {"cpd1_c": "CCO-IN"}
    assert rxn.products == {"cpd2_c": 1}
    assert rxn.product_compartments == {"cpd2_c": "CCO-IN"}
    assert rxn.gibbs0 == -10
    assert rxn.reversible is False
    assert rxn.bounds == (0, 1000)
