from __future__ import annotations

from cycparser import Reaction
from cycparser.repairing.fix_set_reaction_stoichiometry import (
    fix_set_reaction_stoichiometry,
)


def test_set_stoichiometry_basic_case() -> None:
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            products={"cpd2_c": 1},
        )
    }
    reactions = fix_set_reaction_stoichiometry(parse_reactions=reactions)
    assert reactions["RXN1"].stoichiometries == {"cpd1_c": -1, "cpd2_c": 1}


def test_set_stoichiometry_remove_duplicates_products() -> None:
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -2},
            products={"cpd1_c": 1, "cpd2_c": 1},
        )
    }
    reactions = fix_set_reaction_stoichiometry(parse_reactions=reactions)
    assert reactions["RXN1"].stoichiometries == {"cpd1_c": -1, "cpd2_c": 1}


def test_set_stoichiometry_remove_duplicates_substrates() -> None:
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1, "cpd2_c": -1},
            products={"cpd1_c": 2},
        )
    }
    reactions = fix_set_reaction_stoichiometry(parse_reactions=reactions)
    assert reactions["RXN1"].stoichiometries == {"cpd2_c": -1, "cpd1_c": 1}
