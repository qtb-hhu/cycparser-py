from __future__ import annotations

from cycparser import (
    COMPARTMENT_MAP,
    COMPARTMENT_SUFFIXES,
    MANUAL_ADDITIONS,
    Compound,
    Reaction,
    repair,
)
from cycparser.data import Result


def test_repair() -> None:
    compounds = {
        "cpd1_c": Compound(
            id="cpd1",
            base_id="cpd1",
            charge=1,
            formula={"C": 6},
            compartment="CYTOSOL",
            types=["T1"],
        ),
        "cpd2_c": Compound(
            id="cpd2",
            base_id="cpd2",
            charge=1,
            formula={"C": 6},
            compartment="CYTOSOL",
            types=["T2"],
        ),
        "cpd3_c": Compound(
            id="cpd3",
            base_id="cpd3",
            charge=2,
            formula={"C": 6},
            compartment="CYTOSOL",
            types=["T3"],
        ),
        "cpd4_c": Compound(
            id="cpd4",
            base_id="cpd4",
            charge=1,
            formula={"C": 3},
            compartment="CYTOSOL",
            types=["T4"],
        ),
    }
    reactions = {
        "RXN-PASS": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="LEFT-TO-RIGHT",
            locations=[],
            bounds=(0, 1000),
        ),
        "RXN-fail-on-cpd-existence": Reaction(
            id="RXN2",
            base_id="RXN2",
            substrates={"cpdX_c": -1},
            substrate_compartments={"cpdX_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="LEFT-TO-RIGHT",
            locations=[],
            bounds=(0, 1000),
        ),
        "RXN-fail-on-charge-balance": Reaction(
            id="RXN3",
            base_id="RXN3",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd3_c": 1},
            product_compartments={"cpd3_c": "CCO-IN"},
            gibbs0=-10,
            direction="LEFT-TO-RIGHT",
            locations=[],
            bounds=(0, 1000),
        ),
        "RXN-fail-on-mass-balance": Reaction(
            id="RXN4",
            base_id="RXN4",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd4_c": 1},
            product_compartments={"cpd4_c": "CCO-IN"},
            gibbs0=-10,
            direction="LEFT-TO-RIGHT",
            locations=[],
            bounds=(0, 1000),
        ),
        "RXN-create-variants": Reaction(
            id="RXN5",
            base_id="RXN5",
            substrates={"T1_c": -1},
            substrate_compartments={"T1_c": "CCO-IN"},
            products={"T2_c": 1},
            product_compartments={"T2_c": "CCO-IN"},
            gibbs0=-10,
            direction="LEFT-TO-RIGHT",
            locations=[],
            bounds=(0, 1000),
        ),
        "RXN-create-compartment-variants": Reaction(
            id="RXN6",
            base_id="RXN6",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd1_c": 1},
            product_compartments={"cpd1_c": "CCO-OUT"},
            gibbs0=-10,
            direction="LEFT-TO-RIGHT",
            locations=["CCO-EXTRACELLULAR-CCO-CYTOSOL"],
            bounds=(0, 1000),
        ),
        "RXN-create-compartment-variants-fail-on-stoichiometry": Reaction(
            id="RXN7",
            base_id="RXN7",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd1_c": 1},
            product_compartments={"cpd1_c": "CCO-OUT"},
            gibbs0=-10,
            direction="LEFT-TO-RIGHT",
            locations=["CCO-CYTOSOL-CCO-CYTOSOL"],
            bounds=(0, 1000),
        ),
    }

    parse_result = Result(compounds, reactions, {}, {}, {}, {})
    cyc = repair(parse_result, COMPARTMENT_MAP, MANUAL_ADDITIONS, COMPARTMENT_SUFFIXES)

    assert set([i.id for i in cyc.reactions.values()]) == {
        "RXN1_c",
        "RXN6_c_e",
        "RXN5__var__0_c",
    }
