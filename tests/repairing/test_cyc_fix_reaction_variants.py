from __future__ import annotations

from typing import Dict, List

from cycparser import Compound, Reaction, fix_create_reaction_variants


def test_variants_no_variants() -> None:
    compounds = {
        "cpd1_c": Compound(
            id="cpd1_c",
            base_id="cpd1",
            charge=1,
            formula={"C": 1},
        ),
        "cpd2_c": Compound(
            id="cpd2_c",
            base_id="cpd2",
            charge=1,
            formula={"C": 1},
        ),
    }
    compound_types: Dict[str, List[str]] = {}
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
        )
    }
    reactions = fix_create_reaction_variants(
        rxns=reactions, cpds=compounds, compound_types=compound_types
    )

    rxn = reactions["RXN1"]
    assert rxn.id == "RXN1"
    assert rxn.substrates == {"cpd1_c": -1}
    assert rxn.substrate_compartments == {"cpd1_c": "CCO-IN"}
    assert rxn.products == {"cpd2_c": 1}
    assert rxn.product_compartments == {"cpd2_c": "CCO-IN"}


def test_variants_empty() -> None:
    compounds = {
        "A1": Compound(id="A1_c", base_id="A1", formula={"C": 1}, charge=1),
        "A2": Compound(id="A2_c", base_id="A2", formula={"C": 1}, charge=1),
        "B1": Compound(id="B1_c", base_id="B1", formula={"C": 1}, charge=1),
        "B2": Compound(id="B2_c", base_id="B2", formula={"C": 1}, charge=1),
    }
    compound_types: Dict[str, List[str]] = {"T1": [], "T2": []}
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"T1": -1},
            substrate_compartments={"T1": "CCO-IN"},
            products={"T2": 1},
            product_compartments={"T2": "CCO-IN"},
        )
    }
    reactions = fix_create_reaction_variants(
        rxns=reactions, cpds=compounds, compound_types=compound_types
    )
    assert reactions == {}


def test_variants_one_substrate() -> None:
    compounds = {
        "cpd1_c": Compound(
            id="cpd1_c",
            base_id="cpd1",
            charge=1,
            formula={"C": 1},
        ),
        "cpd2_c": Compound(
            id="cpd2_c",
            base_id="cpd2",
            charge=1,
            formula={"C": 1},
        ),
    }
    compound_types: Dict[str, List[str]] = {"T1": ["cpd1_c"]}
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"T1": -1},
            substrate_compartments={"T1": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
        )
    }
    reactions = fix_create_reaction_variants(
        rxns=reactions, cpds=compounds, compound_types=compound_types
    )

    rxn = reactions["RXN1__var__0"]
    assert rxn.id == "RXN1__var__0"
    assert rxn.substrates == {"cpd1_c": -1}
    assert rxn.substrate_compartments == {"cpd1_c": "CCO-IN"}
    assert rxn.products == {"cpd2_c": 1}
    assert rxn.product_compartments == {"cpd2_c": "CCO-IN"}


def test_variants_two_substrates() -> None:
    compounds = {
        "X1": Compound(id="X1", base_id="X1_c", formula={"C": 1}, charge=1),
        "X2": Compound(id="X2", base_id="X2_c", formula={"C": 1}, charge=1),
        "cpd2_c": Compound(id="cpd2_c", base_id="cpd2", charge=1, formula={"C": 1}),
    }
    compound_types: Dict[str, List[str]] = {"T1": ["X1", "X2"]}
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"T1": -1},
            substrate_compartments={"T1": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
        )
    }
    reactions = fix_create_reaction_variants(
        rxns=reactions, cpds=compounds, compound_types=compound_types
    )

    rxn = reactions["RXN1__var__0"]
    assert rxn.id == "RXN1__var__0"
    assert rxn.substrates == {"X1": -1}
    assert rxn.substrate_compartments == {"X1": "CCO-IN"}
    assert rxn.products == {"cpd2_c": 1}
    assert rxn.product_compartments == {"cpd2_c": "CCO-IN"}

    rxn = reactions["RXN1__var__1"]
    assert rxn.id == "RXN1__var__1"
    assert rxn.substrates == {"X2": -1}
    assert rxn.substrate_compartments == {"X2": "CCO-IN"}
    assert rxn.products == {"cpd2_c": 1}
    assert rxn.product_compartments == {"cpd2_c": "CCO-IN"}


def test_variants_one_product() -> None:
    compounds = {
        "cpd1_c": Compound(
            id="cpd1_c",
            base_id="cpd1",
            charge=1,
            formula={"C": 1},
        ),
        "cpd2_c": Compound(
            id="cpd2_c",
            base_id="cpd2",
            charge=1,
            formula={"C": 1},
        ),
    }
    compound_types: Dict[str, List[str]] = {"T2": ["cpd2_c"]}
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"T2": 1},
            product_compartments={"T2": "CCO-IN"},
        )
    }
    reactions = fix_create_reaction_variants(
        rxns=reactions, cpds=compounds, compound_types=compound_types
    )

    rxn = reactions["RXN1__var__0"]
    assert rxn.id == "RXN1__var__0"
    assert rxn.substrates == {"cpd1_c": -1}
    assert rxn.substrate_compartments == {"cpd1_c": "CCO-IN"}
    assert rxn.products == {"cpd2_c": 1}
    assert rxn.product_compartments == {"cpd2_c": "CCO-IN"}


def test_variants_two_products() -> None:
    compounds = {
        "cpd1_c": Compound(
            id="cpd1_c",
            base_id="cpd1",
            charge=1,
            formula={"C": 1},
        ),
        "Y1": Compound(base_id="Y1", id="Y1_c", formula={"C": 1}, charge=1),
        "Y2": Compound(base_id="Y2", id="Y2_c", formula={"C": 1}, charge=1),
    }
    compound_types: Dict[str, List[str]] = {"T2": ["Y1", "Y2"]}
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"T2": 1},
            product_compartments={"T2": "CCO-IN"},
        )
    }
    reactions = fix_create_reaction_variants(
        rxns=reactions, cpds=compounds, compound_types=compound_types
    )

    rxn = reactions["RXN1__var__0"]
    assert rxn.id == "RXN1__var__0"
    assert rxn.substrates == {"cpd1_c": -1}
    assert rxn.substrate_compartments == {"cpd1_c": "CCO-IN"}
    assert rxn.products == {"Y1": 1}
    assert rxn.product_compartments == {"Y1": "CCO-IN"}

    rxn = reactions["RXN1__var__1"]
    assert rxn.id == "RXN1__var__1"
    assert rxn.substrates == {"cpd1_c": -1}
    assert rxn.substrate_compartments == {"cpd1_c": "CCO-IN"}
    assert rxn.products == {"Y2": 1}
    assert rxn.product_compartments == {"Y2": "CCO-IN"}


def test_variants_one_substrate_one_product() -> None:
    compounds = {
        "cpd1_c": Compound(id="cpd1_c", base_id="cpd1", charge=1, formula={"C": 1}),
        "cpd2_c": Compound(id="cpd2_c", base_id="cpd2", charge=1, formula={"C": 1}),
    }
    compound_types: Dict[str, List[str]] = {"T1": ["cpd1_c"], "T2": ["cpd2_c"]}
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"T1": -1},
            substrate_compartments={"T1": "CCO-IN"},
            products={"T2": 1},
            product_compartments={"T2": "CCO-IN"},
        )
    }
    reactions = fix_create_reaction_variants(
        rxns=reactions, cpds=compounds, compound_types=compound_types
    )

    rxn = reactions["RXN1__var__0"]
    assert rxn.id == "RXN1__var__0"
    assert rxn.substrates == {"cpd1_c": -1}
    assert rxn.substrate_compartments == {"cpd1_c": "CCO-IN"}
    assert rxn.products == {"cpd2_c": 1}
    assert rxn.product_compartments == {"cpd2_c": "CCO-IN"}


def test_variants_two_substrates_two_products() -> None:
    compounds = {
        "X1": Compound(base_id="X1", id="X1", formula={"C": 1}, charge=1),
        "X2": Compound(base_id="X2", id="X2", formula={"C": 1}, charge=1),
        "Y1": Compound(base_id="Y1", id="Y1", formula={"C": 1}, charge=1),
        "Y2": Compound(base_id="Y2", id="Y2", formula={"C": 1}, charge=1),
    }
    compound_types: Dict[str, List[str]] = {
        "T1": ["X1", "X2"],
        "T2": ["Y1", "Y2"],
    }
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"T1": -1},
            substrate_compartments={"T1": "CCO-IN"},
            products={"T2": 1},
            product_compartments={"T2": "CCO-IN"},
        )
    }
    reactions = fix_create_reaction_variants(
        rxns=reactions, cpds=compounds, compound_types=compound_types
    )

    rxn = reactions["RXN1__var__0"]
    assert rxn.id == "RXN1__var__0"
    assert rxn.substrates == {"X1": -1}
    assert rxn.substrate_compartments == {"X1": "CCO-IN"}
    assert rxn.products == {"Y1": 1}
    assert rxn.product_compartments == {"Y1": "CCO-IN"}

    rxn = reactions["RXN1__var__1"]
    assert rxn.id == "RXN1__var__1"
    assert rxn.substrates == {"X1": -1}
    assert rxn.substrate_compartments == {"X1": "CCO-IN"}
    assert rxn.products == {"Y2": 1}
    assert rxn.product_compartments == {"Y2": "CCO-IN"}

    rxn = reactions["RXN1__var__2"]
    assert rxn.id == "RXN1__var__2"
    assert rxn.substrates == {"X2": -1}
    assert rxn.substrate_compartments == {"X2": "CCO-IN"}
    assert rxn.products == {"Y1": 1}
    assert rxn.product_compartments == {"Y1": "CCO-IN"}

    rxn = reactions["RXN1__var__3"]
    assert rxn.id == "RXN1__var__3"
    assert rxn.substrates == {"X2": -1}
    assert rxn.substrate_compartments == {"X2": "CCO-IN"}
    assert rxn.products == {"Y2": 1}
    assert rxn.product_compartments == {"Y2": "CCO-IN"}


def test_variants_two_substrates_two_products_one_normal() -> None:
    compounds = {
        "cpd1_c": Compound(id="cpd1_c", base_id="cpd1", charge=1, formula={"C": 1}),
        "cpd2_c": Compound(id="cpd2_c", base_id="cpd2", charge=1, formula={"C": 1}),
        "A1": Compound(base_id="A1", id="A1", formula={"C": 1}, charge=1),
        "A2": Compound(base_id="A2", id="A2", formula={"C": 1}, charge=1),
        "B1": Compound(base_id="B1", id="B1", formula={"C": 1}, charge=1),
        "B2": Compound(base_id="B2", id="B2", formula={"C": 1}, charge=1),
    }
    compound_types: Dict[str, List[str]] = {
        "T1": ["A1", "A2"],
        "T2": ["B1", "B2"],
    }
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1, "T1": -1},
            substrate_compartments={"cpd1_c": "CCO-IN", "T1": "CCO-IN"},
            products={"cpd2_c": 1, "T2": 1},
            product_compartments={"cpd2_c": "CCO-IN", "T2": "CCO-IN"},
        )
    }
    reactions = fix_create_reaction_variants(
        rxns=reactions, cpds=compounds, compound_types=compound_types
    )

    rxn = reactions["RXN1__var__0"]
    assert rxn.id == "RXN1__var__0"
    assert rxn.substrates == {"cpd1_c": -1, "A1": -1}
    assert rxn.substrate_compartments == {"cpd1_c": "CCO-IN", "A1": "CCO-IN"}
    assert rxn.products == {"cpd2_c": 1, "B1": 1}
    assert rxn.product_compartments == {"cpd2_c": "CCO-IN", "B1": "CCO-IN"}

    rxn = reactions["RXN1__var__1"]
    assert rxn.id == "RXN1__var__1"
    assert rxn.substrates == {"cpd1_c": -1, "A1": -1}
    assert rxn.substrate_compartments == {"cpd1_c": "CCO-IN", "A1": "CCO-IN"}
    assert rxn.products == {"cpd2_c": 1, "B2": 1}
    assert rxn.product_compartments == {"cpd2_c": "CCO-IN", "B2": "CCO-IN"}

    rxn = reactions["RXN1__var__2"]
    assert rxn.id == "RXN1__var__2"
    assert rxn.substrates == {"cpd1_c": -1, "A2": -1}
    assert rxn.substrate_compartments == {"cpd1_c": "CCO-IN", "A2": "CCO-IN"}
    assert rxn.products == {"cpd2_c": 1, "B1": 1}
    assert rxn.product_compartments == {"cpd2_c": "CCO-IN", "B1": "CCO-IN"}

    rxn = reactions["RXN1__var__3"]
    assert rxn.id == "RXN1__var__3"
    assert rxn.substrates == {"cpd1_c": -1, "A2": -1}
    assert rxn.substrate_compartments == {"cpd1_c": "CCO-IN", "A2": "CCO-IN"}
    assert rxn.products == {"cpd2_c": 1, "B2": 1}
    assert rxn.product_compartments == {"cpd2_c": "CCO-IN", "B2": "CCO-IN"}


def test_variants_charge_matching() -> None:
    compounds = {
        "A1": Compound(base_id="A1", id="A1", formula={"C": 1}, charge=1),
        "A2": Compound(base_id="A2", id="A2", formula={"C": 1}, charge=0),
        "B1": Compound(base_id="B1", id="B1", formula={"C": 1}, charge=1),
        "B2": Compound(base_id="B2", id="B2", formula={"C": 1}, charge=0),
    }
    compound_types: Dict[str, List[str]] = {
        "T1": ["A1", "A2"],
        "T2": ["B1", "B2"],
    }
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"T1": -1},
            substrate_compartments={"T1": "CCO-IN"},
            products={"T2": 1},
            product_compartments={"T2": "CCO-IN"},
        )
    }
    reactions = fix_create_reaction_variants(
        rxns=reactions, cpds=compounds, compound_types=compound_types
    )

    rxn = reactions["RXN1__var__0"]
    assert rxn.id == "RXN1__var__0"
    assert rxn.substrates == {"A1": -1}
    assert rxn.substrate_compartments == {"A1": "CCO-IN"}
    assert rxn.products == {"B1": 1}
    assert rxn.product_compartments == {"B1": "CCO-IN"}

    rxn = reactions["RXN1__var__1"]
    assert rxn.id == "RXN1__var__1"
    assert rxn.substrates == {"A2": -1}
    assert rxn.substrate_compartments == {"A2": "CCO-IN"}
    assert rxn.products == {"B2": 1}
    assert rxn.product_compartments == {"B2": "CCO-IN"}


def test_variants_formula_matching() -> None:
    compounds = {
        "A1": Compound(base_id="A1", id="A1", formula={"C": 1, "H": 1}, charge=1),
        "A2": Compound(base_id="A2", id="A2", formula={"C": 1}, charge=1),
        "B1": Compound(base_id="B1", id="B1", formula={"C": 1, "H": 1}, charge=1),
        "B2": Compound(base_id="B2", id="B2", formula={"C": 1}, charge=1),
    }
    compound_types: Dict[str, List[str]] = {
        "T1": ["A1", "A2"],
        "T2": ["B1", "B2"],
    }
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"T1": -1},
            substrate_compartments={"T1": "CCO-IN"},
            products={"T2": 1},
            product_compartments={"T2": "CCO-IN"},
        )
    }
    reactions = fix_create_reaction_variants(
        rxns=reactions, cpds=compounds, compound_types=compound_types
    )

    rxn = reactions["RXN1__var__0"]
    assert rxn.id == "RXN1__var__0"
    assert rxn.substrates == {"A1": -1}
    assert rxn.substrate_compartments == {"A1": "CCO-IN"}
    assert rxn.products == {"B1": 1}
    assert rxn.product_compartments == {"B1": "CCO-IN"}

    rxn = reactions["RXN1__var__1"]
    assert rxn.id == "RXN1__var__1"
    assert rxn.substrates == {"A2": -1}
    assert rxn.substrate_compartments == {"A2": "CCO-IN"}
    assert rxn.products == {"B2": 1}
    assert rxn.product_compartments == {"B2": "CCO-IN"}


def test_variants_missing_substrate() -> None:
    compounds = {
        "A1": Compound(base_id="A1", id="A1", formula={"C": 1}, charge=1),
        "B1": Compound(base_id="B1", id="B1", formula={"C": 1}, charge=1),
        "B2": Compound(base_id="B2", id="B2", formula={"C": 1}, charge=1),
    }
    compound_types: Dict[str, List[str]] = {
        "T1": ["A1", "A2"],
        "T2": ["B1", "B2"],
    }
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"T1": -1},
            substrate_compartments={"T1": "CCO-IN"},
            products={"T2": 1},
            product_compartments={"T2": "CCO-IN"},
        )
    }
    reactions = fix_create_reaction_variants(
        rxns=reactions, cpds=compounds, compound_types=compound_types
    )

    rxn = reactions["RXN1__var__0"]
    assert rxn.id == "RXN1__var__0"
    assert rxn.substrates == {"A1": -1}
    assert rxn.substrate_compartments == {"A1": "CCO-IN"}
    assert rxn.products == {"B1": 1}
    assert rxn.product_compartments == {"B1": "CCO-IN"}

    rxn = reactions["RXN1__var__1"]
    assert rxn.id == "RXN1__var__1"
    assert rxn.substrates == {"A1": -1}
    assert rxn.substrate_compartments == {"A1": "CCO-IN"}
    assert rxn.products == {"B2": 1}
    assert rxn.product_compartments == {"B2": "CCO-IN"}


def test_variants_missing_product() -> None:
    compounds = {
        "A1": Compound(base_id="A1", id="A1", formula={"C": 1}, charge=1),
        "A2": Compound(base_id="B1", id="B1", formula={"C": 1}, charge=1),
        "B1": Compound(base_id="B2", id="B2", formula={"C": 1}, charge=1),
    }
    compound_types: Dict[str, List[str]] = {
        "T1": ["A1", "A2"],
        "T2": ["B1", "B2"],
    }
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"T1": -1},
            substrate_compartments={"T1": "CCO-IN"},
            products={"T2": 1},
            product_compartments={"T2": "CCO-IN"},
        )
    }
    reactions = fix_create_reaction_variants(
        rxns=reactions, cpds=compounds, compound_types=compound_types
    )

    rxn = reactions["RXN1__var__0"]
    assert rxn.id == "RXN1__var__0"
    assert rxn.substrates == {"A1": -1}
    assert rxn.substrate_compartments == {"A1": "CCO-IN"}
    assert rxn.products == {"B1": 1}
    assert rxn.product_compartments == {"B1": "CCO-IN"}

    rxn = reactions["RXN1__var__1"]
    assert rxn.id == "RXN1__var__1"
    assert rxn.substrates == {"A2": -1}
    assert rxn.substrate_compartments == {"A2": "CCO-IN"}
    assert rxn.products == {"B1": 1}
    assert rxn.product_compartments == {"B1": "CCO-IN"}
