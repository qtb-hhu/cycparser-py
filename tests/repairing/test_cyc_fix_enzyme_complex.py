from __future__ import annotations

from cycparser.data import Enzyme, Reaction
from cycparser.repairing import get_gpr_associations


def test_get_gpr_associations_single_monomer() -> None:
    enzymes = {
        "ENZRXN-CPLX-1": Enzyme(id="ENZRXN-CPLX-1", cplx_or_monomer="MONOMER-1"),
    }
    reactions = {"rxn1": Reaction("rxn1", "rxn1", enzrxns={"ENZRXN-CPLX-1"})}
    gpr = get_gpr_associations(reactions, enzymes, {})
    gpr["rxn1"] == [{"MONOMER-1"}]
    assert gpr["rxn1"] == [{"MONOMER-1"}]


def test_get_gpr_associations_complex_single_monomer() -> None:
    complexes = {
        "CPLX-1": {"MONOMER-1"},
    }
    enzymes = {
        "ENZRXN-CPLX-1": Enzyme(id="ENZRXN-CPLX-1", cplx_or_monomer="CPLX-1"),
    }
    reactions = {"rxn1": Reaction("rxn1", "rxn1", enzrxns={"ENZRXN-CPLX-1"})}
    gpr = get_gpr_associations(reactions, enzymes, complexes)
    assert gpr["rxn1"] == [{"MONOMER-1"}]


def test_get_gpr_associations_complex_two_monomers() -> None:
    complexes = {
        "CPLX-1": {"MONOMER-1", "MONOMER-2"},
    }
    enzymes = {
        "ENZRXN-CPLX-1": Enzyme(id="ENZRXN-CPLX-1", cplx_or_monomer="CPLX-1"),
    }
    reactions = {"rxn1": Reaction("rxn1", "rxn1", enzrxns={"ENZRXN-CPLX-1"})}
    gpr = get_gpr_associations(reactions, enzymes, complexes)
    assert gpr["rxn1"] == [{"MONOMER-1", "MONOMER-2"}]


def test_get_gpr_associations_complex_of_complex() -> None:
    complexes = {
        "CPLX-1": {"CPLX-2"},
        "CPLX-2": {"MONOMER-1"},
    }
    enzymes = {
        "ENZRXN-CPLX-1": Enzyme(id="ENZRXN-CPLX-1", cplx_or_monomer="CPLX-1"),
    }
    reactions = {"rxn1": Reaction("rxn1", "rxn1", enzrxns={"ENZRXN-CPLX-1"})}
    gpr = get_gpr_associations(reactions, enzymes, complexes)
    assert gpr["rxn1"] == [{"MONOMER-1"}]


def test_get_gpr_associations_complex_of_mixed() -> None:
    complexes = {
        "CPLX-1": {"CPLX-2", "MONOMER-1"},
        "CPLX-2": {"MONOMER-2"},
    }
    enzymes = {
        "ENZRXN-CPLX-1": Enzyme(id="ENZRXN-CPLX-1", cplx_or_monomer="CPLX-1"),
    }
    reactions = {"rxn1": Reaction("rxn1", "rxn1", enzrxns={"ENZRXN-CPLX-1"})}
    gpr = get_gpr_associations(reactions, enzymes, complexes)
    assert gpr["rxn1"] == [{"MONOMER-1", "MONOMER-2"}]


def test_get_gpr_associations_multiple() -> None:
    complexes = {
        "CPLX-1": {"MONOMER-1"},
        "CPLX-2": {"MONOMER-2"},
    }
    enzymes = {
        "ENZRXN-CPLX-1": Enzyme(id="ENZRXN-CPLX-1", cplx_or_monomer="CPLX-1"),
        "ENZRXN-CPLX-2": Enzyme(id="ENZRXN-CPLX-2", cplx_or_monomer="CPLX-2"),
    }
    reactions = {
        "rxn1": Reaction("rxn1", "rxn1", enzrxns={"ENZRXN-CPLX-1", "ENZRXN-CPLX-2"})
    }
    gpr = get_gpr_associations(reactions, enzymes, complexes)

    # Order isn't guaranteed
    assert {"MONOMER-1"} in gpr["rxn1"]
    assert {"MONOMER-2"} in gpr["rxn1"]
