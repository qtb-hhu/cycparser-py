from __future__ import annotations

from cycparser import Compound, Reaction
from cycparser.utils import (
    check_charge_balance,
    check_compound_existence,
    check_mass_balance,
)


def test_compound_existence() -> None:
    compounds = {
        "cpd1_c": Compound(id="cpd2_c", base_id="cpd2", compartment="CYTOSOL"),
        "cpd2_c": Compound(id="cpd2_c", base_id="cpd2", compartment="CYTOSOL"),
    }

    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    assert check_compound_existence(reactions["RXN1"], compounds)


def test_missing_substrate() -> None:
    compounds = {"cpd2_c": Compound(id="cpd2_c", base_id="cpd2", compartment="CYTOSOL")}
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    assert not check_compound_existence(reactions["RXN1"], compounds)


def test_missing_product() -> None:
    compounds = {"cpd1_c": Compound(id="cpd2_c", base_id="cpd2", compartment="CYTOSOL")}
    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    assert not check_compound_existence(reactions["RXN1"], compounds)


def test_charge_balance_both_zero() -> None:
    compounds = {
        "cpd1_c": Compound(id="cpd1_c", base_id="cpd1", compartment="CYTOSOL"),
        "cpd2_c": Compound(id="cpd2_c", base_id="cpd2", compartment="CYTOSOL"),
    }

    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    assert check_charge_balance(reactions["RXN1"], compounds)


def test_charge_balance_both_one() -> None:
    compounds = {
        "cpd1_c": Compound(id="cpd1_c", base_id="cpd1", charge=1, compartment="CYTOSOL"),
        "cpd2_c": Compound(id="cpd2_c", base_id="cpd2", charge=1, compartment="CYTOSOL"),
    }

    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    assert check_charge_balance(reactions["RXN1"], compounds)


def test_charge_balance_substrate_stoichiometry() -> None:
    compounds = {
        "cpd1_c": Compound(id="cpd1_c", base_id="cpd1", charge=1, compartment="CYTOSOL"),
        "cpd2_c": Compound(id="cpd2_c", base_id="cpd2", charge=2, compartment="CYTOSOL"),
    }

    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -2},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    assert check_charge_balance(reactions["RXN1"], compounds)


def test_charge_balance_product_stoichiometry() -> None:
    compounds = {
        "cpd1_c": Compound(id="cpd1_c", base_id="cpd1", charge=2, compartment="CYTOSOL"),
        "cpd2_c": Compound(id="cpd2_c", base_id="cpd2", charge=1, compartment="CYTOSOL"),
    }

    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 2},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    assert check_charge_balance(reactions["RXN1"], compounds)


def test_charge_balance_opposite_signs() -> None:
    compounds = {
        "cpd1_c": Compound(id="cpd1_c", base_id="cpd1", charge=-1, compartment="CYTOSOL"),
        "cpd2_c": Compound(id="cpd2_c", base_id="cpd2", charge=1, compartment="CYTOSOL"),
    }

    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    assert not check_charge_balance(reactions["RXN1"], compounds)


def test_mass_balance_single_atom() -> None:
    compounds = {
        "cpd1_c": Compound(id="cpd1_c", base_id="cpd1", formula={"C": 1}),
        "cpd2_c": Compound(id="cpd2_c", base_id="cpd2", formula={"C": 1}),
    }

    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    assert check_mass_balance(reactions["RXN1"], compounds)


def test_mass_balance_multiple_atoms() -> None:
    compounds = {
        "cpd1_c": Compound(
            id="cpd1_c",
            base_id="cpd1",
            formula={"C": 6, "H": 12, "O": 6},
        ),
        "cpd2_c": Compound(
            id="cpd2_c",
            base_id="cpd2",
            formula={"C": 6, "H": 12, "O": 6},
        ),
    }

    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    assert check_mass_balance(reactions["RXN1"], compounds)


def test_mass_balance_multiple_compounds() -> None:
    compounds = {
        "cpd1_c": Compound(
            id="cpd1_c",
            base_id="cpd1",
            formula={"C": 6, "H": 12, "O": 6},
        ),
        "cpd2_c": Compound(
            id="cpd2_c",
            base_id="cpd2",
            formula={"C": 6, "H": 12, "O": 6},
        ),
        "A_c": Compound(
            id="A_c",
            base_id="A",
            formula={"C": 6},
        ),
        "B_c": Compound(
            id="B_c",
            base_id="B",
            formula={"C": 6},
        ),
    }

    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1, "A_c": -2},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1, "B_c": 2},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    assert check_mass_balance(reactions["RXN1"], compounds)


def test_mass_balance_multiple_atoms_substrate_stoichiometry() -> None:
    compounds = {
        "cpd1_c": Compound(
            id="cpd1_c",
            base_id="cpd1",
            formula={"C": 3, "H": 6, "O": 3},
        ),
        "cpd2_c": Compound(
            id="cpd2_c",
            base_id="cpd2",
            formula={"C": 6, "H": 12, "O": 6},
        ),
    }

    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -2},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    assert check_mass_balance(reactions["RXN1"], compounds)


def test_mass_balance_multiple_atoms_product_stoichiometry() -> None:
    compounds = {
        "cpd1_c": Compound(
            id="cpd1_c",
            base_id="cpd1",
            formula={"C": 6, "H": 12, "O": 6},
        ),
        "cpd2_c": Compound(
            id="cpd2_c",
            base_id="cpd2",
            formula={"C": 3, "H": 6, "O": 3},
        ),
    }

    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 2},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    assert check_mass_balance(reactions["RXN1"], compounds)


def test_mass_balance_missing_substrate_formula() -> None:
    compounds = {
        "cpd1_c": Compound(
            id="cpd1_c",
            base_id="cpd1",
            formula={},
        ),
        "cpd2_c": Compound(
            id="cpd2_c",
            base_id="cpd2",
            formula={
                "C": 1,
            },
        ),
    }

    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    assert not check_mass_balance(reactions["RXN1"], compounds)


def test_mass_balance_missing_product_formula() -> None:
    compounds = {
        "cpd1_c": Compound(
            id="cpd1_c",
            base_id="cpd1",
            formula={"C": 1},
        ),
        "cpd2_c": Compound(
            id="cpd2_c",
            base_id="cpd2",
            formula={},
        ),
    }

    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    assert not check_mass_balance(reactions["RXN1"], compounds)


def test_mass_balance_missing_substrate_atom() -> None:
    compounds = {
        "cpd1_c": Compound(
            id="cpd1_c",
            base_id="cpd1",
            formula={"C": 1},
        ),
        "cpd2_c": Compound(
            id="cpd2_c",
            base_id="cpd2",
            formula={"C": 1, "H": 1},
        ),
    }

    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    assert not check_mass_balance(reactions["RXN1"], compounds)


def test_mass_balance_missing_product_atom() -> None:
    compounds = {
        "cpd1_c": Compound(
            id="cpd1_c",
            base_id="cpd1",
            formula={"C": 1, "H": 1},
        ),
        "cpd2_c": Compound(
            id="cpd2_c",
            base_id="cpd2",
            formula={"C": 1},
        ),
    }

    reactions = {
        "RXN1": Reaction(
            id="RXN1",
            base_id="RXN1",
            substrates={"cpd1_c": -1},
            substrate_compartments={"cpd1_c": "CCO-IN"},
            products={"cpd2_c": 1},
            product_compartments={"cpd2_c": "CCO-IN"},
            gibbs0=-10,
            direction="IRREVERSIBLE-RIGHT-TO-LEFT",
            bounds=(0, 1000),
        )
    }
    assert not check_mass_balance(reactions["RXN1"], compounds)
