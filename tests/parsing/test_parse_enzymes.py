from __future__ import annotations

from cycparser.parsing import parse_enzymes


def test_read_single() -> None:
    enzrxns = parse_enzymes(["UNIQUE-ID - ENZRXN-13149", "ENZYME - MONOMER-17831"])
    assert enzrxns["ENZRXN-13149"].cplx_or_monomer == "MONOMER-17831"
