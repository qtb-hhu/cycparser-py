from __future__ import annotations

from cycparser.parsing import parse_compounds


def test_create_compound() -> None:
    cpds = parse_compounds(type_map={}, file=["UNIQUE-ID - cpd1"])
    assert cpds["cpd1_c"].id == "cpd1_c"


def test_create_default_compartment() -> None:
    cpds = parse_compounds(type_map={}, file=["UNIQUE-ID - cpd1"])
    assert cpds["cpd1_c"].compartment == "CYTOSOL"


def test_create_default_charge() -> None:
    cpds = parse_compounds(type_map={}, file=["UNIQUE-ID - cpd1"])
    assert cpds["cpd1_c"].charge == 0


def test_atom_charges_neg() -> None:
    cpds = parse_compounds(
        type_map={},
        file=[
            "UNIQUE-ID - cpd1",
            "ATOM-CHARGES - (0 -1)",
        ],
    )
    assert cpds["cpd1_c"].charge == -1.0


def test_atom_charges_pos() -> None:
    cpds = parse_compounds(
        type_map={},
        file=[
            "UNIQUE-ID - cpd1",
            "ATOM-CHARGES - (0 1)",
        ],
    )
    assert cpds["cpd1_c"].charge == 1.0


def test_formula_single() -> None:
    cpds = parse_compounds(
        type_map={},
        file=[
            "UNIQUE-ID - cpd1",
            "CHEMICAL-FORMULA - (C 1)",
        ],
    )
    assert cpds["cpd1_c"].formula == {"C": 1}


def test_formula_multiple() -> None:
    cpds = parse_compounds(
        type_map={},
        file=[
            "UNIQUE-ID - cpd1",
            "CHEMICAL-FORMULA - (C 6)",
            "CHEMICAL-FORMULA - (H 12)",
            "CHEMICAL-FORMULA - (O 6)",
        ],
    )
    assert cpds["cpd1_c"].formula == {"C": 6, "H": 12, "O": 6}


def test_formula_two_letters() -> None:
    cpds = parse_compounds(
        type_map={}, file=["UNIQUE-ID - cpd1", "CHEMICAL-FORMULA - (He 1)"]
    )
    assert cpds["cpd1_c"].formula == {"He": 1}


def test_formula_multiple_two_letters() -> None:
    cpds = parse_compounds(
        type_map={},
        file=[
            "UNIQUE-ID - cpd1",
            "CHEMICAL-FORMULA - (C 6)",
            "CHEMICAL-FORMULA - (He 12)",
            "CHEMICAL-FORMULA - (O 6)",
        ],
    )
    assert cpds["cpd1_c"].formula == {"C": 6, "He": 12, "O": 6}


def test_smiles() -> None:
    cpds = parse_compounds(type_map={}, file=["UNIQUE-ID - cpd1", "SMILES - COC1"])
    assert cpds["cpd1_c"].smiles == "COC1"


def test_atom_charges_pos_and_neg() -> None:
    cpds = parse_compounds(
        type_map={},
        file=[
            "UNIQUE-ID - cpd1",
            "ATOM-CHARGES - (0 -1)",
            "ATOM-CHARGES - (1 1)",
        ],
    )
    assert cpds["cpd1_c"].charge == 0
