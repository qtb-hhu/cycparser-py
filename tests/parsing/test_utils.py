from __future__ import annotations

from cycparser.parsing import remove_top_comments
from cycparser.parsing.shared import _rename


def test_remove_top_comments() -> None:
    file = [
        "# Species: MetaCyc",
        "# Database: MetaCyc",
        "# Version: 23.0",
        "# File Name: reactions.dat",
        "# Date and time generated: November 20, 2018, 14:01:26",
        "#",
        "# Attributes:",
        "#    UNIQUE-ID",
        "#    TYPES",
        "#    COMMON-NAME",
        "#    ATOM-MAPPINGS",
        "#    SYSTEMATIC-NAME",
        "#    TAXONOMIC-RANGE",
        "#",
        "UNIQUE-ID - RXN-19177",
    ]

    file = remove_top_comments(file)
    assert ["UNIQUE-ID - RXN-19177"] == file


def test_rename() -> None:
    assert "O-methylbergaptol" == _rename("<i>O</i>-methylbergaptol")
    assert "N2-acetyl-ornithine" == _rename("<i>N<sup>2</sup></i>-acetyl-ornithine")
    assert "SePO3" == _rename("SePO<sub>3</sub>")
    assert "UDP-2,4-diacetamido-2,4,6-trideoxy-alpha-D-glucopyranose" == _rename(
        "UDP-2,4-diacetamido-2,4,6-trideoxy-&alpha;-D-glucopyranose"
    )
    assert "Glucopyranose" == _rename("|Glucopyranose|")
