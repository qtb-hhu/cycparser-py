from __future__ import annotations

from cycparser.parsing import parse_sequences


def test_read_multiple() -> None:
    sequences = parse_sequences(
        iter(
            [
                ">gnl|META|HS10525-MONOMER Serine--pyruvate aminotransferase (Homo sapiens)",
                "MASHED",
                ">gnl|META|HS10520-MONOMER alpha-(1,3)-fucosyltransferase 9 (Homo sapiens)",
                "POTATOES",
            ]
        )
    )
    assert sequences["HS10525-MONOMER"] == "MASHED"
    assert sequences["HS10520-MONOMER"] == "POTATOES"
