Changelog
=========


1.2.112 (2024-12-22)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.111 (2024-12-15)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.110 (2024-12-08)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.109 (2024-12-01)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated pre-commit [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.108 (2024-11-24)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.107 (2024-11-17)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.106 (2024-11-10)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated pre-commit [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.105 (2024-10-20)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.104 (2024-10-13)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated pre-commit [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.103 (2024-10-06)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.102 (2024-09-29)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated pre-commit [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.101 (2024-09-22)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.100 (2024-09-15)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.99 (2024-09-08)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.98 (2024-09-01)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.97 (2024-08-25)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.96 (2024-08-11)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.95 (2024-08-04)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated pre-commit [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.94 (2024-07-25)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.2.93 (2024-07-22)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [Dan Howe]


1.2.92 (2024-07-17)
-------------------

Changes
~~~~~~~
- updated dependencies [Dan Howe]
- updated changelog [Marvin van Aalst]


1.2.91 (2024-07-01)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.90 (2024-06-24)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.89 (2024-06-17)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.88 (2024-06-10)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.87 (2024-06-03)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.86 (2024-05-27)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.85 (2024-05-20)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.84 (2024-05-06)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.83 (2024-05-03)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.82 (2024-04-08)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.81 (2024-04-01)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.80 (2024-03-25)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.79 (2024-03-18)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.78 (2024-03-11)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.77 (2024-02-26)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.76 (2024-02-12)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.75 (2024-02-05)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.74 (2024-01-29)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.73 (2024-01-15)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.72 (2024-01-08)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.71 (2024-01-01)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.70 (2023-12-25)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.69 (2023-12-20)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.68 (2023-12-11)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.67 (2023-11-13)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.66 (2023-11-06)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.65 (2023-10-30)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.64 (2023-10-16)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.63 (2023-10-09)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.62 (2023-10-02)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.61 (2023-09-25)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.60 (2023-09-18)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.59 (2023-09-11)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.58 (2023-09-04)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.57 (2023-08-28)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.56 (2023-08-21)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.55 (2023-08-14)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.54 (2023-08-07)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.53 (2023-07-31)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.52 (2023-07-24)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.51 (2023-07-17)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.50 (2023-07-10)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.49 (2023-07-03)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.48 (2023-06-26)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.47 (2023-06-19)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.46 (2023-06-12)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.45 (2023-06-05)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.44 (2023-05-30)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.43 (2023-05-15)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.42 (2023-05-08)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.41 (2023-04-11)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.40 (2023-03-31)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.39 (2023-03-14)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.38 (2023-03-13)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.37 (2023-03-06)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.36 (2023-02-06)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.35 (2023-01-30)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.34 (2023-01-23)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.33 (2023-01-16)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.32 (2023-01-09)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.31 (2023-01-02)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.30 (2022-12-27)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.29 (2022-12-19)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.28 (2022-12-12)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.27 (2022-11-29)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.26 (2022-11-21)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.25 (2022-11-21)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.24 (2022-11-14)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.23 (2022-11-07)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.22 (2022-10-31)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.21 (2022-10-26)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.20 (2022-10-24)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.19 (2022-09-27)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.17 (2022-09-26)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.16 (2022-09-19)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.15 (2022-09-12)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.14 (2022-09-05)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.13 (2022-08-29)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.12 (2022-08-22)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.11 (2022-08-15)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.10 (2022-08-08)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.9 (2022-08-01)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.8 (2022-07-18)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.7 (2022-07-11)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.6 (2022-07-08)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.5 (2022-06-27)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.4 (2022-06-20)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.3 (2022-06-13)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.2 (2022-06-07)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.2.1 (2022-05-30)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.1.0 (2022-05-25)
------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]
- updated dependencies [Marvin van Aalst]


1.0.21 (2022-05-23)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.0.19 (2022-05-16)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.0.18 (2022-05-09)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.0.16 (2022-04-18)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.0.15 (2022-04-11)
-------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.0.14 (2022-04-11)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.0.13 (2022-03-28)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.0.12 (2022-03-21)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.0.11 (2022-03-07)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.0.10 (2022-02-28)
-------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.0.9 (2022-02-28)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.0.8 (2022-02-21)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.0.7 (2022-02-14)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.0.6 (2022-02-09)
------------------

New
~~~
- warn if one of the database files doesn't exist [Marvin van Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.0.5 (2022-02-07)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.0.4 (2022-01-31)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.0.3 (2022-01-24)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.0.2 (2022-01-17)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.0.0 (2022-01-13)
------------------

New
~~~
- Version 1.0 [Marvin van Aalst]


